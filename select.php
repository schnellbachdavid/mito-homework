<?php require_once 'header.php'; ?>

<div class="grid">
    <div class="select__info">
        <a href="/"><img src="assets/img/svg/plus.svg" alt=""></a>
        <div>
            <small>Leaving from</small>
            <h3>Budapest</h3>
        </div>
        <img src="assets/img/svg/double-arrows.svg" class="px-2">
        <div>
            <small>&nbsp;</small>
            <h3>Barcelona El Prat</h3>
        </div>
    </div>
    <div class="select__headline">
        <img src="assets/img/svg/plane.svg" alt="">
        <h2>Select Flight</h2>
    </div>
    <div class="select__sidebar">
        
        <div class="flight__sum">
            <div class="flight__sum__top">
                Flights
                <span>$0</span>
            </div>
            <div class="flight__sum__info">
                Choose an outbound flight
            </div>
            <div class="flight__sum__content">
                <div class="flight__sum__ticket">
                    <div>
                        <span>Nov</span>
                        3
                    </div>
                    <div>
                        Budapest – Barcelona El Prat
                        <small>Wed 06:02 – 07:35</small>
                    </div>
                </div>
                <div class="flight__sum__ticket">
                    <div>
                        <span>Nov</span>
                        4
                    </div>
                    <div>
                        Barcelona El Prat - Budapest
                        <small>Wed 06:02 – 07:35</small>
                    </div>
                </div>
            </div>
            <div class="flight__sum__total">
                Total
                <span>$0</span>
            </div>
        </div>
        <div class="btn btn-secondary disabled mx-auto">Pay Now</div>

    </div>
    <div class="select__main">
        
        <div class="flight">
            <div class="flight__top">
                Outbound
                <div>
                    Budapest 
                    <img src="assets/img/svg/arrow-primary.svg" alt="">
                    Barcelona El Prat
                </div>
            </div>
            <div class="flight__date">
                <div>
                    <img src="assets/img/svg/arrow-left.svg" alt="">
                    WED 7 OCTOBER
                </div>
                <div>
                    Saturday, 3 November 2015
                </div>
                <div>
                    sat 10 october
                    <img src="assets/img/svg/arrow-right.svg" alt="">
                </div>
            </div>
            <div class="flight__content">
                <div class="flight__row flight__row--header">
                    <div></div>
                    <div>
                        Basic
                    </div>
                    <div>
                        Standard
                    </div>
                    <div>
                        Plus
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="flight">
            <div class="flight__top">
                Inbound
                <div>
                    Barcelona El Prat
                    <img src="assets/img/svg/arrow-primary.svg" alt="">
                    Budapest 
                </div>
            </div>
            <div class="flight__date">
                <div>
                    <img src="assets/img/svg/arrow-left.svg" alt="">
                    WED 7 OCTOBER
                </div>
                <div>
                    Saturday, 3 November 2015
                </div>
                <div>
                    sat 10 october
                    <img src="assets/img/svg/arrow-right.svg" alt="">
                </div>
            </div>
            <div class="flight__content">
                <div class="flight__row flight__row--header">
                    <div></div>
                    <div>
                        Basic
                    </div>
                    <div>
                        Standard
                    </div>
                    <div>
                        Plus
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
                <div class="flight__row">
                    <div>
                        06:02
                        <img src="assets/img/svg/arrow.svg" alt="">
                        07:35
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$9.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$19.99</span>
                    </div>
                    <div>
                        <span class="btn btn-secondary-outline">$29.99</span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php require_once 'footer.php'; ?>
