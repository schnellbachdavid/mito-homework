![Alt text](http://stylus-lang.com/img/stylus-logo.svg)    

## Workflow használat:
0. Live reload telepítése: https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en
1. Node.js telepítése, hogy legyen npm (nodejs.org)
2. npm install -global gulp-cli
3. npm install --> (package.json-ben lévő dependenciák telepítése)
4. gulp taskok futtatása
5. START: npm run watch
* * *

## Gulp taskok:
* buildVendors - vendor css és js fordítása
* default - saját css és js fordítása
* lint - linter futtatása, hiba report-al
* watch
* * *

## Linterek és autofix:
* eslint - `yarn run eslint` || `npm run eslint`
* eslint automatikus javítással - `yarn run eslint-fix` || `npm run eslint-fix`
* stylus linter - `gulp lint`
* stylus linter automatikus javítással - `yarn run stylus-fix` || `npm run stylus-fix`

## CSS Vendor package-ek:
* Reset CSS

## JS Vendor pluginok:
* jQuery 3.3.1 [https://jquery.com/]
* SmoothScroll for websites v1.4.10 [https://github.com/gblazex/smoothscroll-for-websites]

## Stylus mappastruktúra
css
├── layout
│   └── 01_variables.styl
│   └── 02_mixins.styl
│   └── 03_type.styl
│   └── 04_forms.styl
│   └── 05_buttons.styl
│   └── 06_layout.styl
├── modules
│   └── header.styl
│   └── footer.styl
├── vendor
│   └── 01_reset-2.0.css
└── style.styl