function initInputLabelEffect() {
    $('.input-container input').each(function () {
        if ($(this).val() !== '') {
            $(this).parent().find('label').addClass('is-focused');
        } else {
            $(this).parent().find('label').removeClass('is-focused');
        }
    });
}

(function($) {
    $(() => {

	    // INPUT FOCUS EFFECT INIT
        initInputLabelEffect();

        // INPUT FOCUS EFFECT
        $('.input-container input').focusin(function () {
            $('.input-container.is-focused').removeClass('is-focused');
            $(this).parents('.input-container--error').removeClass('input-container--error');

            $('.input-container input').each(function () {
                if ($(this).val() === '') {
                    $(this).parent().find('label').removeClass('is-focused');
                }
            });

            $(this).closest('.input-container').addClass('is-focused');
            $(this).parent().find('label').addClass('is-focused');
        });

        // CHECK EMPTY INPUTS TO REMOVE IN FOCUS EFFECTS
	    $(document).mouseup(function (e) {
	        var container = $('.input-container input');
	        if (!container.is(e.target) && container.has(e.target).length === 0) {
	            initInputLabelEffect()
	        }
	    });        

    });
})(jQuery);
