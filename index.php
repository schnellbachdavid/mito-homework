<?php require_once 'header.php'; ?>

<div class="modal__backdrop">
    <div class="modal">
        <div class="modal__header">
            <p>Thanks for buying your tickets at mito airlines</p>
        </div>
        <div class="modal__body">
            <div class="flight__sum__ticket">
                <div>
                    <span>Nov</span>
                    3
                </div>
                <div>
                    Budapest – Barcelona El Prat
                    <small>Wed 06:02 – 07:35</small>
                </div>
            </div>
            <div class="flight__sum__ticket">
                <div>
                    <span>Nov</span>
                    4
                </div>
                <div>
                    Barcelona El Prat - Budapest
                    <small>Wed 06:02 – 07:35</small>
                </div>
            </div>
        </div>
        <div class="modal__footer">
            <div>
                Total: <span>$9.99</span>
            </div>
            <div>
                <small>NO, THANKS (RESET)</small>
            </div>
        </div>
    </div>
</div>

<section class="home">
    <form class="box box--sm" action="/select.php">
        <div class="box__top box__top--primary">
            <img src="assets/img/svg/plus.svg" alt="">
            Mito Airline
        </div>
        <div class="box__content">
            <div class="box__row">
                <div class="box__row__half">
                    <div class="input-container">
                        <label for="origin">Origin</label>
                        <input type="text" id="origin">
                        <ul>
                        </ul>
                        <div class="input-container__error-text">
                            Error text
                        </div>
                    </div>
                </div>
                <div class="box__row__half">
                    <div class="input-container">
                        <label for="destination">Destination</label>
                        <input type="text" id="destination">
                        <ul>
                        </ul>
                        <div class="input-container__error-text">
                            Error text
                        </div>
                    </div>
                </div>
            </div>
            <div class="box__row">
                <div class="box__row__half">
                    <div class="input-container input-container--calendar input-container--error">
                        <label for="departure">Departure</label>
                        <input type="text" id="departure">
                        <div class="input-container__error-text">
                            Please select departure
                        </div>
                    </div>
                </div>
                <div class="box__row__half">
                    <div class="input-container input-container--calendar">
                        <label for="return">Return</label>
                        <input type="text" id="return">
                        <div class="input-container__error-text">
                            Error text
                        </div>
                    </div>
                </div>
            </div>
            <div class="box__row">
                <input class="btn btn-primary" type="submit" value="Search">
            </div>
        </div>
    </form>
</section>

<?php require_once 'footer.php'; ?>
